import android.content.Context;
import java.util.Locale;
import java.util.List;
import java.util.Date;
import java.text.SimpleDateFormat;
import ketai.sensors.*;

// Location variables
double longitude, latitude, altitude, accuracy;
KetaiLocation location;
String address ; // Will hold the city name provided by openweathermap

// Apps fonts
PFont TinyFont;
PFont SmallFont;
PFont MedFont;
PFont BigFont;

// Some variables used for the colour gradient
int Y_AXIS = 1;
int X_AXIS = 2;

// OpenWeather.org variables
String openweather = "http://api.openweathermap.org/data/2.5/" ;
String apiid;

// Some color variables
color colorCold = color(0, 100, 150);
color colorHot = color(255, 100, 0);
int decal = 183;

//Temperature range for colour interpolation
float Tmin = -15.0;
float Tmax = 35.0;
float max_intens = 50.0;

//
boolean showGrad = false;
int refresh = 10*60*1000;
int lastTime = -refresh-10;
boolean showForecast= false;

Weather W;
ArrayList Drops = new ArrayList(); //a bucket to store the drops

class Weather {
  float [] temp = new float[9];
  int [] rain_mode = new int[9];
  float [] rain_intens = new float[9];
  Date [] date =  new Date[9];

  Weather (float[] _temp, int[] _rain_mode, float[] _rain_intens, Date[] _date) {
    for (int i = 0; i < 9; i++) {
      temp[i] = _temp[i];
      rain_mode[i] = _rain_mode[i];
      rain_intens[i] = _rain_intens[i];
      date[i] = _date[i];
    }
  }
  float getTemp(int d) {
    return temp[d];
  }
  float getRain_intens(int d) {
    return rain_intens[d];
  }
  int getRain_mode(int d) {
    return rain_mode[d];
  }
  Date getDate(int d) {
    return date[d];
  }
}

class Drop {
  PVector pos;//positon of the drop
  float vel;//velocity of the drop
  float size;
  int type;
  int idx;

  Drop (PVector _pos, float _vel, int _type, int _idx) {//function--"constructor"-makes the drop
    pos = _pos;//take the incoming information and store it
    vel = _vel;
    type = _type;
    idx = _idx;
    if (type == 2) {
      size = 30;
    } else {
      size = 1;
    }
  }
  void render(int _idx) {
    if (idx == _idx) {
      if (type == 1) {
        noFill();//fill the drop
      } else {
        fill(color(255, 255-size));
      }
      stroke(color(255, 255-size));
      if  (type == 1) {
        ellipse(pos.x, pos.y, size, size);//draw the drop on the screen
      } else {
        ellipse(pos.x, pos.y, 30, 30);//draw the drop on the screen
      }
      noStroke();
    }
  }
  float grow(int _idx) {//make drops move
    if (idx == _idx) {
      size=size+ vel;//add the velocity vector to our position
    }
    return size;
  }
}

Weather getWeather(double latitude, double longitude) {
  String current;
  String forecast;
  String owm_weather;
  String owm_forecast;
  float []Temp = new float[9];
  float[]Rain = new float[9];
  int []tRain = new int[9];
  Date[] Date = new Date[9];

  //owm_weather = openweather+"weather?q="+Address+"&APPID="+apiid+"&mode=xml";
  //owm_forecast = openweather+"forecast/daily?q="+Address+"&APPID="+apiid+"&mode=xml&cnt=8";
  owm_weather = openweather+"weather?lat="+latitude+"&lon="+longitude+"&APPID="+apiid+"&mode=xml";
  owm_forecast = openweather+"forecast/daily?lat="+latitude+"&lon="+longitude+"&APPID="+apiid+"&mode=xml&cnt=8";

  current = join(loadStrings(owm_weather), "");
  forecast = join(loadStrings(owm_forecast), "");

  //Working on current Weather
  XML results = parseXML(current);
  XML child_city = results.getChild("city");
  address = child_city.getString("name");
  XML child_day = results.getChild("lastupdate");
  //  Date[0] = child_day.getString("value");
  try {
    Date tempDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(child_day.getString("value"));
    Date[0] = tempDate;
  }
  catch(java.text.ParseException e) {
    println("Unable to parse date strings:" );
  }
  XML child_temp = results.getChild("temperature");
  Temp[0] = child_temp.getFloat("value") - 273.15;
  XML child_rain = results.getChild("precipitation");
  if (child_rain.getString("mode").equals("rain")) {
    Rain[0] = child_rain.getFloat("value");
    tRain[0] = 1;
  } else {
    if (child_rain.getString("mode").equals("snow")) {
      Rain[0] = child_rain.getFloat("value");
      tRain[0] = 2;
    } else {
      Rain[0] = 0;
      tRain[0] = 0;
    }
  }

  //Working on forecast weather
  XML results2 = parseXML(forecast);
  XML child = results2.getChild("forecast");
  XML[] day_forecast = child.getChildren("time");
  for (int i = 0; i < day_forecast.length; i++) {
    //   Date[i+1] =  day_forecast[i].getString("day");
    try {
      Date tempDate = new SimpleDateFormat("yyyy-MM-dd").parse(day_forecast[i].getString("day"));
      Date[i+1] = tempDate;
    }
    catch(java.text.ParseException e) {
      println("Unable to parse date strings:" );
    }
    XML day_temp = day_forecast[i].getChild("temperature");
    Temp[i+1] = day_temp.getFloat("day") - 273.15;
    XML day_rain =  day_forecast[i].getChild("precipitation");
    if (day_rain.getString("type") != null) {
      if (day_rain.getString("type").equals("rain")) {
        Rain[i+1] = day_rain.getFloat("value");
        tRain[i+1] = 1;
      }
      if (day_rain.getString("type").equals("snow")) {
        Rain[i+1] = child_rain.getFloat("value");
        tRain[i+1] = 2;
      }
    } else {
      Rain[i+1] = 0;
      tRain[i+1] = 0;
    }
  }
  Weather W = new Weather(Temp, tRain, Rain, Date);

  return(W);
 }

color setColor(float T) {
  color actualColor;
  if ( T >= 0.0) {
    actualColor = lerpColor(color(165, 184, 199), colorHot, (T - 0)/(Tmax - 0));
  } else {
    actualColor = lerpColor(colorCold, color(164, 184, 199), (T - Tmin)/(0 - Tmin));
  }
  return actualColor;
}

void rain(float intens, int tRain, int idx, int Width, int Height) {
  float size;

  if (random(1) < intens/max_intens) {
    Drop newD = new Drop(new PVector(random(Width), random(Height)), 1, tRain, idx);
    Drops.add(newD);
  }
  if (Drops.size()> 0) {
    for (int i = 0; i < Drops.size (); i++) {
      Drop D = (Drop)Drops.get(i); //get one Drop out
      size = D.grow(idx); //move the drop
      D.render(idx); //draw the drop
      if (size > 255) {
        Drops.remove(i);
      }
    }
  }
}

void onResume() {
  location = new KetaiLocation(this);
  super.onResume();
}

void onLocationEvent(Location _location) {
  //print out the location object
  println("onLocation event: " + _location.toString());
  longitude = _location.getLongitude();
  latitude = _location.getLatitude();
  altitude = _location.getAltitude();
  accuracy = _location.getAccuracy();
}

void legendBar(int Width, int Height, color textCol) {
  if (Width > displayWidth) {
    Width = displayWidth;
  }
  textFont(SmallFont);
  int size = 1;
  float Tvar = Tmin;
  for (int i = 0; i <= Width; i++) {
    Tvar = Tvar + (Tmax -Tmin)/((float) Width);
    fill(setColor(Tvar));
    noStroke();
    rect(size*i, 0, size, 70);
  }
  for (float i = Tmin +5; i < Tmax; i=i+5) {
    fill(textCol);
    stroke(textCol);
    text((int)i, Width*(i-Tmin)/(Tmax-Tmin), Height/2);
    line(Width*(i-Tmin)/(Tmax-Tmin), 0, Width*(i-Tmin)/(Tmax-Tmin), 10);
    line(Width*(i-Tmin)/(Tmax-Tmin), Height-10, Width*(i-Tmin)/(Tmax-Tmin), Height);
  }
}

void setGradient(int x, int y, float w, float h, color c1, color c2, int axis ) {

  noFill();

  if (axis == Y_AXIS) {  // Top to bottom gradient
    for (int i = y; i <= y+h; i++) {
      float inter = map(i, y, y+h, 0, 1);
      color c = lerpColor(c1, c2, inter);
      stroke(c);
      line(x, i, x+w, i);
    }
  }
  else if (axis == X_AXIS) {  // Left to right gradient
    for (int i = x; i <= x+w; i++) {
      float inter = map(i, x, x+w, 0, 1);
      color c = lerpColor(c1, c2, inter);
      stroke(c);
      line(i, y, i, y+h);
    }
  }
}

void setup() {
  fullScreen();
  orientation(PORTRAIT);

  TinyFont = createFont("SansSerif", 24, false);
  SmallFont = createFont("SansSerif", 56, false);
  MedFont = createFont("SansSerif", 64, false);
  BigFont = createFont("SansSerif", 256, false);
  textFont(SmallFont);

  apiid=loadStrings("api.txt")[0];
  translate(0, 0);
}

void draw() {
  location.getProvider();
  translate(0, 0);
  if (millis() > lastTime + refresh) {
    W =getWeather(latitude,longitude);
    lastTime = millis();
  }

  if (mousePressed) {
    if (mouseY > 1000) {
      showForecast = !showForecast;
      delay(100);
    }
  }
  if (showForecast) {
    drawForecast(W);
  } else {
    drawMain(W);
  }
}

void drawMain(Weather W) {
  SimpleDateFormat formater = null;

  formater = new SimpleDateFormat("dd/MM");
  textAlign(CENTER, CENTER);
  background(setColor(W.getTemp(0)));

  if ( W.getRain_mode(1)> 0) {
    rain(W.getRain_intens(1), W.getRain_mode(1), 0, displayWidth, displayHeight);
  }
  textFont(SmallFont);
  text(address, displayWidth/2, 50);
  textFont(BigFont);
  stroke(color(255, 255, 255));
  text(nfc(W.getTemp(0), 1)+"°", displayWidth/2, displayHeight/3);
  textFont(MedFont);
  if ( W.getRain_intens(1)> 0) {
    text(nfc(W.getRain_intens(1), 1)+"mm", displayWidth/2, displayHeight/3+150);
  }
  setGradient(0, 6*183,100.0,  100.0, color(0), color(255), Y_AXIS);

  translate(0, 6*183);
  textAlign(LEFT, CENTER);

  fill(setColor(W.getTemp(0)));
  noStroke();
  rect(0, 0, displayWidth, 182);
  textFont(MedFont);
  fill(color(255));
  text(formater.format(W.getDate(1)), 20, decal/2);
  textAlign(RIGHT, CENTER);

  text(nfc(W.getTemp(1), 1)+"°", displayWidth -20, decal/3-10);
  if ( W.getRain_mode(0)> 0) {
    rain(W.getRain_intens(1), W.getRain_mode(1), 1, displayWidth, decal);
    fill(color(255));
    textFont(SmallFont);
    text(nfc(W.getRain_intens(0), 1)+"mm", displayWidth -20, 2*decal/3+10);
  }
}


void drawForecast(Weather W) {
  SimpleDateFormat formater = null;

  formater = new SimpleDateFormat("dd/MM");
  translate(0, 0);
  noStroke();
  for (int i = 2; i < 9; i++) {
    textAlign(LEFT, CENTER);
    fill(setColor(W.getTemp(i)));

    if (i < 8) {
      //     colorNext = setColor(W.getTemp(i+1));
      //     setGradient(0,0,displayWidth,decal,actualColor,colorNext,Y_AXIS);
    }
    rect(0, 0, displayWidth, decal);
    textFont(MedFont);
    fill(color(255));
    text(formater.format(W.getDate(i)), 20, decal/2);
    textAlign(RIGHT, CENTER);

    text(nfc(W.getTemp(i), 1)+"°", displayWidth -20, decal/3-10);
    translate(0, decal);
  }
  translate(0, -7*decal);
  for (int i = 2; i < 9; i++) {
    if ( W.getRain_mode(i)> 0) {
      rain(W.getRain_intens(i), W.getRain_mode(i), i, displayWidth, decal);
      fill(color(255));
      textFont(SmallFont);
      textAlign(RIGHT, CENTER);
      text(nfc(W.getRain_intens(i), 1)+"mm", displayWidth -20, 2*decal/3+10);
    }
    translate(0, decal);
  }
}
